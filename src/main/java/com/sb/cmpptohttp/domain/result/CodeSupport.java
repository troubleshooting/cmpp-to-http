package com.sb.cmpptohttp.domain.result;


public interface CodeSupport {

  int code();

  String msg();
}
